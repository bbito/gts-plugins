This fork was started to migrate gts.DatePicker to Enyo 2.7 there is no commitment at this point for work on the other kinds. [glitchtech-science's original Enyo 2.2 DatePicker jsFiddle](http://jsfiddle.net/glitchtechscience/SCknQ/) was also hacked to work with Enyo 2.7: [http://jsfiddle.net/buckbito/o3ntt2Lh/3/](http://jsfiddle.net/buckbito/o3ntt2Lh/3/)

Original GlitchTechScience ReadMe info below:

GTS Library
===========

This library contains kinds for use with Enyo (http://enyojs.com)

This library does contain a deploy script, but only for systems that use deploy.sh.

For changes, please see the commit logs.

Support
-------

Email Address: glitchtechscience@gmail.com

Website: http://forums.webosnation.com/glitchtech-science/